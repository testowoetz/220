<?
class Sklad
	{
	private $pda_sql = NULL;
	private $returndata = array();
	
	function add_item($item_title,$item_price,$item_short_description,$brend_id)
		{
		$this->pda_sql = new pda_sql(TRUE,FALSE);
		$query_arr = array();
		array_push($query_arr,"INSERT INTO `220v`.`items` (`item_id`,
				`item_title`,
				`item_price`,
				`item_short_description`,
				`brend_id`)
			VALUES (NULL,
				".$this->pda_sql->secure($item_title).",
				".$this->pda_sql->secure($item_price).",
				".$this->pda_sql->secure($item_short_description).",
				".$this->pda_sql->secure($brend_id).")");
		array_push($query_arr,'LAST_INSERT_ID');
		
		if(($item_id = $this->pda_sql->multitrans_query($query_arr)) !== FALSE)
			{
			$this->returndata['item_id'] = $item_id[0];
			$this->output();
			}
		else
			{
			$this->output(3);
			}
		}
	
	function remove_item($item_id)
		{
		$this->pda_sql = new pda_sql(TRUE,FALSE);
		
		if($this->pda_sql->w_query("DELETE FROM `220v`.`items`
			WHERE `items`.`item_id`=".$this->pda_sql->secure($item_id)) !== FALSE)
			{
			$this->returndata['removed_item_id'] = $item_id;
			$this->output();
			}
		else
			{
			$this->output(3);
			}
		}
	
	function add_brend($brend_name)
		{
		$this->pda_sql = new pda_sql(TRUE,FALSE);
		$query_arr = array();
		array_push($query_arr,"INSERT INTO `220v`.`brends` (`brend_id`,
				`brend_name`)
			VALUES (NULL,
				".$this->pda_sql->secure($brend_name).")");
		array_push($query_arr,'LAST_INSERT_ID');
		
		if(($brend_id = $this->pda_sql->multitrans_query($query_arr)) !== FALSE)
			{
			$this->returndata['brend_id'] = $brend_id[0];
			$this->output();
			}
		else
			{
			if($this->pda_sql->error_number == 1062)
				{
				$this->output(6);
				}
			else
				{
				$this->output(3);
				}
			}
		}
	
	function remove_brend($brend_id)
		{
		$this->pda_sql = new pda_sql(TRUE,FALSE);
		
		if($this->pda_sql->w_query("DELETE FROM `220v`.`brends`
			WHERE `brends`.`brend_id`=".$this->pda_sql->secure($brend_id)) !== FALSE)
			{
			$this->returndata['removed_brend_id'] = $brend_id;
			$this->output();
			}
		else
			{
			$this->output(3);
			}
		}
	
	function get_brends_with_min_items($min_items)
		{
		$this->pda_sql = new pda_sql();
		
		if($this->returndata['brends'] = $this->pda_sql->read_assoc("SELECT `brends`.`brend_id`,`brends`.`brend_name`
			FROM `220v`.`brends`
			WHERE `brends`.`brend_id` in (SELECT `items`.`brend_id`
				FROM `220v`.`items`
				GROUP BY `items`.`brend_id` having count(*) >= ".$this->pda_sql->secure($min_items).")"))
			{
			$this->output();
			}
		else
			{
			if($this->returndata['brends'] === NULL)
				{
				$this->output(4);
				}
			else
				{
				$this->output(3);
				}
			}
		}
	
	function get_items_from_brend($brend_id)
		{
		$this->pda_sql = new pda_sql();
		
		if($this->returndata['items'] = $this->pda_sql->read_assoc("SELECT `items`.*
			FROM `220v`.`items`
			WHERE `items`.`brend_id` = ".$this->pda_sql->secure($brend_id)))
			{
			foreach($this->returndata['items'] as $key => $query)
				{
				$this->returndata['items'][$key]['price_in_other_currencies'] = array('USD' => number_format((($this->returndata['items'][$key]['item_price']*$this->get_currency_mem('RUBUSD'))+0),2,'.',','),
					'RUB' => $this->returndata['items'][$key]['item_price'],
					'KZT' => number_format((($this->returndata['items'][$key]['item_price']*$this->get_currency_mem('RUBKZT'))+0),2,'.',','),
					'BYN' => number_format((($this->returndata['items'][$key]['item_price']*$this->get_currency_mem('RUBBYN'))+0),2,'.',','));
				}
			$this->output();
			}
		else
			{
			$this->output(3);
			}
		}
	
	function get_item_list()
		{
		$this->pda_sql = new pda_sql();
		
		return $this->pda_sql->read_assoc("
			SELECT *
			FROM `220v`.`items`
			ORDER BY `items`.`item_title` ASC");
		}
	
	function get_brend_list()
		{
		$this->pda_sql = new pda_sql();
		
		return $this->pda_sql->read_assoc("
			SELECT *
			FROM `220v`.`brends`
			ORDER BY `brends`.`brend_name` ASC");
		}
	
	function check_brend_id($brend_id)
		{
		$this->pda_sql = new pda_sql();
		
		return $this->pda_sql->query_getcell("
			SELECT `brend_id`
			FROM `220v`.`brends`
			WHERE `brends`.`brend_id`=".$this->pda_sql->secure($brend_id));
		}
	
	function check_item_id($item_id)
		{
		$this->pda_sql = new pda_sql();
		
		return $this->pda_sql->query_getcell("
			SELECT `item_id`
			FROM `220v`.`items`
			WHERE `items`.`item_id`=".$this->pda_sql->secure($item_id));
		}
	
	function get_currency($currency_pair)
		{
		if($yahoo_finance_data = file_get_contents('http://download.finance.yahoo.com/d/quotes.csv?s='.$currency_pair.'=X&f=sl1d1t1ba') AND
			$currency_data = explode(',',$yahoo_finance_data) AND
			preg_match('/^\d+(?:\.\d{0,7})?$/',$currency_data[1]))
			{
			return $currency_data[1];
			}
		else
			{
			$this->output(2);
			}
		}
	
	function get_currency_mem($currency_pair)
		{
		if(!($memcached = new Memcached()))
			{
			$currency_rate = $this->get_currency($currency_pair);
			}
		else
			{
			switch($_SERVER['HTTP_HOST'])
				{
				case 'your.domain':
					{
					$mc_servers = array(
						array('your.memcached.server',34118,33));
					break;
					}
				
				case '220.test':
					{
					$mc_servers = array(
						array('localhost',11211,33));
					break;
					}
				}
			
			if(!($memcached->addServers($mc_servers)))
				{
				$currency_rate = $this->get_currency($currency_pair);
				}
			else
				{
				$memcached_key = md5($_SERVER['HTTP_HOST'].$currency_pair);
				
				/*
					Комманда:
						document.cookie = "refresh=nocache; expires=Thu, 20 Dec 2018 12:00:00 UTC";
					набранная в консоли браузера повесит куку, которая будет заставлять идти в обход кэша.
					Оставлено для технических тестов.
				*/
				
				if(isset($_COOKIE['refresh']) AND
					$_COOKIE['refresh'] == 'nocache')
					{
					$getCacheDetail = FALSE;
					}
				else
					{
					$getCacheDetail = $memcached->get($memcached_key);
					}
				
				if($getCacheDetail)
					{
					$currency_rate=$getCacheDetail;
					$set_into_mem = FALSE;
					}
				else
					{
					$currency_rate = $this->get_currency($currency_pair);
					$set_into_mem = TRUE;
					}
				
				if($set_into_mem === TRUE AND
					$currency_rate !== FALSE AND
					$currency_rate !== NULL)
					{
					$memcached->set($memcached_key,$currency_rate,60);
					}
				}
			}
		return $currency_rate;
		}
	
	function output($status=1)
		{
		/*
			status:
				1 - nice
				2 - can not get needed info from other service
				3 - db error
				4 - not found
				5 - wrond data
				6 - allready exists
		*/
		$this->returndata['status'] = $status;
		echo json_encode($this->returndata,JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
		exit();
		}
	}
?>