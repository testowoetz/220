<?
class pda_sql extends mysqli
	{
	private $db_data = array();
	private $w_link = FALSE; //writer connection link
	private $r_link = FALSE; //reader connection link
	private $query_log = TRUE;
	private $mysql_explain_log = TRUE;
	
	function __construct($writer=FALSE,$reader=TRUE)
		{
		switch($_SERVER['HTTP_HOST'])
			{
			case 'your.domain':
				{
				/* MASTER */
				$this->db_data['w_db_server_ip'] = 'your.DB.master.server.com';
				$this->db_data['w_db_server_port'] = 'your.port.number';
				/* REPLICA SLAVE */
				$this->db_data['r_db_server_ip'] = 'your.DB.replica.server.com';
				$this->db_data['r_db_server_port'] = 'your.port.number';
				/* Only "SELECT, INSERT, UPDATE, DELETE" */
				$this->db_data['w_user'] = 'your.username.with.write.rights';
				$this->db_data['w_password'] = 'your.password';
				/* Only "SELECT" */
				$this->db_data['r_user'] = 'your.username.with.readonly.rights';
				$this->db_data['r_password'] = 'your.password';
				break;
				}
			
			case '220.test':
				{
				/* MASTER */
				$this->db_data['w_db_server_ip'] = 'localhost';
				$this->db_data['w_db_server_port'] = '3306';
				/* REPLICA SLAVE */
				$this->db_data['r_db_server_ip'] = 'localhost';
				$this->db_data['r_db_server_port'] = '3306';
				/* Only "SELECT, INSERT, UPDATE, DELETE" */
				$this->db_data['w_user'] = 'db_writer';
				$this->db_data['w_password'] = '7ca61e5637ab5beb2594ff8f439fbb92';
				/* Only "SELECT" */
				$this->db_data['r_user'] = 'db_reader';
				$this->db_data['r_password'] = '182d74c9e6483732648f331d3539d78a';
				break;
				}
			}
		
		if($writer AND !$this->w_link)
			{
			if(($this->w_link = mysqli_connect($this->db_data['w_db_server_ip'],
				$this->db_data['w_user'],
				$this->db_data['w_password'],
				'',
				$this->db_data['w_db_server_port'])) !== FALSE)
				{
				mysqli_query($this->w_link,"SET CHARACTER SET utf8");
				mysqli_query($this->w_link,"SET NAMES utf8");
				}
			else
				{
				$this->error_report($query,$this->w_link);
				return FALSE;
				}
			}
		
		if($reader AND !$this->r_link)
			{
			if(($this->r_link = mysqli_connect($this->db_data['r_db_server_ip'],
				$this->db_data['r_user'],
				$this->db_data['r_password'],
				'',
				$this->db_data['r_db_server_port'])) !== FALSE)
				{
				mysqli_query($this->r_link,"SET CHARACTER SET utf8");
				mysqli_query($this->r_link,"SET NAMES utf8");
				}
			else
				{
				if(($this->r_link = mysqli_connect($this->db_data['w_db_server_ip'],
					$this->db_data['r_user'],
					$this->db_data['r_password'],
					'',
					$this->db_data['w_db_server_port'])) !== FALSE)
					{
					mysqli_query($this->r_link,"SET CHARACTER SET utf8");
					mysqli_query($this->r_link,"SET NAMES utf8");
					}
				else
					{
					$this->error_report($query,$this->r_link);
					return FALSE;
					}
				}
			}
		}
	
	function r_query($query,$log=NULL)
		{
		if($log)
			{
			$this->log_query($query);
			}
		
		if(($answer = mysqli_query($this->r_link,$query)) !== FALSE)
			{
			return $answer;
			}
		else
			{
			$this->error_report($query,$this->r_link);
			return FALSE;
			}
		}
	
	function w_query($query,$log=NULL)
		{
		if($log)
			{
			$this->log_query($query);
			}
		
		if(($answer = mysqli_query($this->w_link,$query)) !== FALSE)
			{
			return $answer;
			}
		else
			{
			$this->error_report($query,$this->w_link);
			return FALSE;
			}
		}
	
	function read_assoc($query,$log=NULL)
		{
		$assoc = array();
		if(($mysql_request = $this->r_query($query,$log)) !== FALSE)
			{
			if($mysql_request->num_rows > 0)
				{
				while($result = $mysql_request->fetch_assoc())
					{
					array_push($assoc,$result);
					}
				return $assoc;
				}
			else
				{
				return NULL;
				}
			}
		else
			{
			return FALSE;
			}
		}
	
	function memcached_read($query,$ttl=15,$log=NULL)
		{
		debug_point('mc fired',__LINE__);
		$assoc = array();
		if(!($memcached = new Memcached()))
			{
			debug_point('mc start failed',__LINE__);
			debug_point('data comes from database',__LINE__);
			$assoc = $this->read_assoc($query,$log);
			}
		else
			{
			debug_point('mc started',__LINE__);
			switch($_SERVER['HTTP_HOST'])
				{
				case 'your.domain':
					{
					$mc_servers = array(
						array('your.memcached.server',34118,33));
					break;
					}
				
				case '220.test':
					{
					$mc_servers = array(
						array('localhost',11211,33));
					break;
					}
				}
			
			if(!($memcached->addServers($mc_servers)))
				{
				$assoc = $this->read_assoc($query,$log);
				}
			else
				{
				$memcached_key = md5($_SERVER['HTTP_HOST'].$query);
				
				/* document.cookie = "refresh=nocache; expires=Thu, 20 Dec 2018 12:00:00 UTC"; */
				if(isset($_COOKIE['refresh']) AND
					$_COOKIE['refresh'] == 'nocache')
					{
					$getCacheDetail = FALSE;
					}
				else
					{
					$getCacheDetail = $memcached->get($memcached_key);
					}
				
				if($getCacheDetail)
					{
					$assoc=$getCacheDetail;
					$set_into_mem = FALSE;
					}
				else
					{
					$assoc = $this->read_assoc($query,$log);
					$set_into_mem = TRUE;
					}
				
				if($set_into_mem === TRUE AND
					$assoc !== FALSE AND
					$assoc !== NULL)
					{
					if(!($memcached->set($memcached_key,$assoc,$ttl)))
						{
						/* report? */
						}
					}
				}
			}
		return $assoc;
		}
		
		
	function query_getcell($query,$log=FALSE,$rw='r')
		{
		switch(TRUE)
			{
			case is_string($query):
				{
				if($rw == 'r')
					{
					$answer = $this->r_query($query,$log);
					}
				else
					{
					$answer = $this->w_query($query,$log);
					}
				
				if($answer !== FALSE)
					{
					if($answer->num_rows == 1)
						{
						$answer = $answer->fetch_row();
						$answer = $answer[0];
						return $answer;
						}
					else
						{
						return NULL;
						}
					}
				else
					{
					return FALSE;
					}
				break;
				}
			
			case is_a($query,'mysqli_result'):
				{
				if($query->num_rows == 1)
					{
					$answer = $query->fetch_row();
					$answer = $answer[0];
					return $answer;
					}
				else
					{
					return NULL;
					}
				break;
				}
			
			default:
				{
				return FALSE;
				break;
				}
			}
		}
	
	function query_getrow($query,$log=FALSE)
		{
		switch(TRUE)
			{
			case is_string($query):
				{
				if(($answer = $this->r_query($query,$log)) !== FALSE)
					{
					if($answer->num_rows == 1)
						{
						$answer = $answer->fetch_assoc();
						return $answer;
						}
					else
						{
						return NULL;
						}
					}
				else
					{
					return FALSE;
					}
				break;
				}
			
			case is_a($query,'mysqli_result'):
				{
				if($query->num_rows == 1)
					{
					$answer = $query->fetch_assoc();
					return $answer;
					}
				else
					{
					return NULL;
					}
				break;
				}
			
			default:
				{
				return FALSE;
				break;
				}
			}
		}
	
	function multitrans_query($query_arr,$log=FALSE)
		{
		if($this->w_query("SET autocommit=0") AND $this->w_query("START TRANSACTION"))
			{
			foreach($query_arr as $key => $query)
				{
				if($query == 'LAST_INSERT_ID')
					{
					if(!isset($LAST_INSERT_ID))
						{
						$LAST_INSERT_ID = array();
						}
					
					if(($new_id = $this->query_getcell("SELECT LAST_INSERT_ID()",$log,'w')) !== FALSE AND $new_id !== NULL AND $new_id > 0)
						{
						array_push($LAST_INSERT_ID,$new_id);
						}
					else
						{
						$this->w_query("ROLLBACK");
						$this->w_query("SET autocommit=1");
						return FALSE;
						}
					}
				else
					{
					if(!$this->w_query($query,$log))
						{
						$this->w_query("ROLLBACK");
						$this->w_query("SET autocommit=1");
						return FALSE;
						}
					}
				}
			
			if($this->w_query("COMMIT") AND
				$this->w_query("SET autocommit=1"))
				{
				if(isset($LAST_INSERT_ID))
					{
					return $LAST_INSERT_ID;
					}
				else
					{
					return TRUE;
					}
				}
			}
		return FALSE;
		}
	
	function secure($value)
		{
		if(get_magic_quotes_gpc())
			{
			$value = stripslashes($value);
			}
		
		if(!is_array($value) AND
			!is_numeric($value) AND
			$value != 'NULL')
			{
			if(($seclink = $this->r_link) !== FALSE OR
				($seclink = $this->w_link) !== FALSE)
				{
				$value = "'".mysqli_real_escape_string($seclink,$value)."'";
				}
			else
				{
				$returndata['status'] = 3;
				}
			}
		else
			{
			if(is_array($value))
				{
				foreach($value as $key => $val)
					{
					$value[$key] = $this->secure($val);
					}
				$value = implode(',',$value);
				}
			}
		return $value;
		}
	
	function log_query($query)
		{
		if($this->query_log)
			{
			$main_path = substr(dirname(__FILE__),0,-3);
			$filename = '../logs/mysql/'.date('d.m.y').'.query';
			if(!file_exists($filename) OR is_writable($filename))
				{
				if($handle = @fopen($filename, 'a'))
					{
					fwrite($handle,$_SERVER["REQUEST_URI"]."\n");
					fwrite($handle,$query."\n\n");
					if($this->mysql_explain_log)
						{
						if($explain = $this->r_query("EXPLAIN EXTENDED ".$query,FALSE) AND $explain->num_rows > 0)
							{
							fwrite($handle,"id\tselect_type\ttable\ttype\tpossible_keys\tkey\tkey_len\tref\trows\tfiltered\tExtra\n");
							while($explain_line = $explain->fetch_assoc())
								{
								fwrite($handle,implode("\t",$explain_line)."\n");
								}
							}
						}
					fwrite($handle,"\n\n\n\n\n\n");
					fclose($handle);
					}
				else
					{
					/* TODO send mail to tech!!! */
					}
				}
			}
		}
		
	function error_report($query,$sql_link)
		{
		$this->error_number = mysqli_errno($sql_link);
		$err = 'file: '.$_SERVER["REQUEST_URI"]."\n";
		$err .= 'query: '.$query."\n";
		$err .= 'mysql_error:'.mysqli_error($sql_link)."\n";
		$err .= 'time: '.date('G:i:s',time());
		$filename = '../logs/mysql/'.date('d.m.y').'.error';
		if(!file_exists($filename) OR is_writable($filename))
			{
			if($handle = fopen($filename, 'a'))
				{
				fwrite($handle, $err."\n\n\n");
				fclose($handle);
				}
			else
				{
					/* TODO send mail to tech!!! */
				}
			}
		}
	
	function secure_output($value)
		{
		return htmlentities($value,ENT_QUOTES,'UTF-8');
		}
	}
?>