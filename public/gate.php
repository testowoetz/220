<?
/* include own library */
require_once '../inc/functions_oop.php';
require_once '../inc/sklad_oop.php';

/* Тестовое задание на бэкэнд, потому,
	чтобы не тратить время на кодинг правильного фронта,
	небольшой костыль ввиду ограничений HTML´a на аттрибутах method тэга <form>*/
if(isset($_POST['_method']) AND
	!empty($_POST['_method']))
	{
	$_SERVER['REQUEST_METHOD'] = $_POST['_method'];
	unset($_POST['_method']);
	}



$request = explode('/', trim($_SERVER['PATH_INFO'],'/'));
$place = preg_replace('/[^a-z0-9_]+/i','',array_shift($request));

$sklad = new Sklad;
switch($_SERVER['REQUEST_METHOD'])
	{
	case 'POST':
		{
		switch($place)
			{
			/* ТЗ - пункт 1*/
			case 'item':
				{
				if(isset($_POST['item_title']) AND
					!empty($_POST['item_title']) AND
					isset($_POST['item_short_description']) AND
					!empty($_POST['item_short_description']) AND
					isset($_POST['item_price']) AND
					!empty($_POST['item_price']) AND
					preg_match('/^\d+(?:\.\d{0,2})?$/',$_POST['item_price']) AND
					isset($_POST['brend_id']) AND
					!empty($_POST['brend_id']) AND
					$sklad->check_brend_id($_POST['brend_id']))
					{
					$sklad->add_item($_POST['item_title'],$_POST['item_price'],$_POST['item_short_description'],$_POST['brend_id']);
					}
				else
					{
					$sklad->output(5);
					}
				break;
				}
			
			/* ТЗ - пункт 3*/
			case 'brend':
				{
				if(isset($_POST['brend_name']) AND
					!empty($_POST['brend_name']))
					{
					$sklad->add_brend($_POST['brend_name']);
					}
				else
					{
					$sklad->output(5);
					}
				break;
				}
			}
		break;
		}
	
	case 'DELETE':
		{
		switch($place)
			{
			/* ТЗ - пункт 2*/
			case 'item':
				{
				if(isset($_POST['item_id']) AND
					!empty($_POST['item_id']) AND
					$sklad->check_item_id($_POST['item_id']))
					{
					$sklad->remove_item($_POST['item_id']);
					}
				else
					{
					$sklad->output(5);
					}
				break;
				}
			
			/* ТЗ - пункт 4*/
			case 'brend':
				{
				if(isset($_POST['brend_id']) AND
					!empty($_POST['brend_id']) AND
					$sklad->check_brend_id($_POST['brend_id']))
					{
					$sklad->remove_brend($_POST['brend_id']);
					}
				else
					{
					$sklad->output(5);
					}
				break;
				}
			}
		break;
		}
	
	case 'GET':
		{
		$id = array_shift($request)+0;
		switch($place)
			{
			/* ТЗ - пункт 5*/
			case 'brend_min':
				{
				if($id > 0)
					{
					$sklad->get_brends_with_min_items($id);
					}
				else
					{
					$sklad->output(5);
					}
				break;
				}
			
			/* ТЗ - пункт 6*/
			case 'brend_items':
				{
				if($sklad->check_brend_id($id))
					{
					$sklad->get_items_from_brend($id);
					}
				else
					{
					$sklad->output(5);
					}
				break;
				}
			}
		break;
		}
	}
?>