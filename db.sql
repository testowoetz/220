CREATE TABLE `brends` (
  `brend_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `brend_name` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`brend_id`),
  UNIQUE KEY `brend_name` (`brend_name`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED KEY_BLOCK_SIZE=4;

CREATE TABLE `items` (
  `item_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_title` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `item_price` decimal(9,2) unsigned NOT NULL,
  `item_short_description` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `brend_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`item_id`),
  KEY `brend_id` (`brend_id`),
  CONSTRAINT `fk_item_brend_id` FOREIGN KEY (`brend_id`) REFERENCES `brends` (`brend_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED KEY_BLOCK_SIZE=4;